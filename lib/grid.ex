defmodule Grid do

  def init(grid) when length(grid) == 2, do: {:ok, grid}
  def init(_), do: {:error, "Invalid grid size!"}
end
