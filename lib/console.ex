defmodule Console do
  @moduledoc """
  Solution for mars rover problem.
  """

  @doc """
  Takes the multiple inputs and returns the output
  """
  @spec start :: String.t()
  def start do
    grid = IO.gets("Enter the grid size(space seperated):")
    grid = Parser.grid_parser(grid)

    case grid do
      {:error, message} ->
        IO.puts(message)
        start

      {:ok, grid} ->
        rover_state = IO.gets("Enter the rover state(space seperated):")
        rover = Parser.rover_parser(rover_state, grid)

        case rover do
          {:error, message} ->
            IO.puts(message)
            start

          {:ok, _} ->
            instructions = IO.gets("Enter the instructions:")
            instructions = instructions |> Parser.instruction_parser

            case instructions do
              {:error, message} ->
                IO.puts(message)
                start

              {:ok, instructions} ->
                final_state = rover |> Rover.execute(instructions, grid)

                case final_state do
                  {:error, message} ->
                    IO.puts(message)
                    start

                  {:ok, data} ->
                    IO.puts("#{data.x} #{data.y} #{data.dir}")
                    start
                end
            end

        end
    end
  end
end
