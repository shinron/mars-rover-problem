defmodule Parser do

  @doc """
  Parses the instruction string and returns an array.

  ## Parameters

      - instructions: string type

    ## Example

    iex> Parser.instruction_parser("LRM")
    {:ok, ["L", "R", "M"]}
    """
  def instruction_parser(instructions) when is_bitstring(instructions) do
    instructions = instructions |> String.trim |> String.codepoints
    {:ok, instructions}
  end
  def instruction_parser(_), do: {:error, "Invalid Instructions!"}

  @doc """
  Parses the space seperated values and returns an array.

  ## Parameters

      - grid: string type

    ## Example

    iex> Parser.grid_parser("5 5")
    {:ok, [5, 5]}
    """
    @spec grid_parser(grid :: String.t()) :: list()
    def grid_parser(grid) when is_bitstring(grid) do
      grid
      |> String.trim
      |> String.split(" ")
      |> Enum.map(&(String.to_integer/1))
      |> Grid.init
    end
    def grid_parser(_), do: {:error, "Invalid grid size!"}

  @doc """
  Parses the space seperated values and returns an array.

  ## Parameters

      - rover_state: string type
      - grid: list type

    ## Example

    iex> Parser.rover_parser("1 2 N", [5, 5])
    {:ok, %{:dir => "N", :x => 1, :y => 2}}
    """
  def rover_parser(rover_state, grid) when is_list(grid) and is_bitstring(rover_state) do
    [x, y, dir] = rover_state |> String.trim |> String.split(" ")
    [x,y] = [x,y] |> Enum.map(&(String.to_integer/1))
    Rover.init(x, y, dir, grid)
  end
  def rover_parser(_, _), do: {:error, "Invalid Rover Coordinates!"}
end
