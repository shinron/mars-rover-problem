defmodule Rover do

  # Defining the constants for Directions & Movements.
  @north "N"
  @south "S"
  @east "E"
  @west "W"
  @left "L"
  @right "R"
  @move "M"

  @doc """
    Initializes the rover

    ## Parameters

      - x coordinate: integer type
      - y coordinate: integer type
      - direction: string type
      - grid: list type

    ## Example

    iex> Rover.init(1, 5, "N", [5, 5])
    {:ok, %{:dir => "N", :x => 1, :y => 5}}
    """
  @spec init(x :: integer(), y :: integer(), dir :: String.t(), grid :: list()) :: tuple()
  def init(x, y, dir, grid) when is_integer(x) and is_integer(y) and is_bitstring(dir) and is_list(grid) and length(grid) == 2 do
    dirs = [@north, @south, @east, @west]
    [row, col] = grid
    if x <= col && y <= row && x >= 0 && y >= 0 do
      cond do
        dir in dirs ->
          {:ok, %{:x => x, :y => y, :dir => dir}}
        true ->
          {:error, "Invalid Direction or Case! Supported directions are 'N', 'S', 'E', 'W'."}
      end
    else
      {:error, "Invalid Rover Coordinates!"}
    end
  end
  def init(_, _, _, _), do: {:error, "Invalid Rover Coordinates!"}

  @doc """
    Rotates the rover

    ## Parameters

      - rover data: tuple type
      - movement: string type
      - grid: list type

    ## Example

    iex> Rover.move({:ok, %{:dir => "E", :x => 1, :y => 5}}, "L", [5, 5])
    {:ok, %{:dir => "N", :x => 1, :y => 5}}
    """
  @spec move(rover :: tuple(), string :: String.t(), grid :: list()) :: tuple()
  def move({:ok, %{:dir => @north}} = rover, @left, grid), do: {:ok, %{elem(rover, 1) | :dir => @west}}
  def move({:ok, %{:dir => @west}} = rover, @left, grid), do: {:ok, %{elem(rover, 1) | :dir => @south}}
  def move({:ok, %{:dir => @south}} = rover, @left, grid), do: {:ok, %{elem(rover, 1) | :dir => @east}}
  def move({:ok, %{:dir => @east}} = rover, @left, grid), do: {:ok, %{elem(rover, 1) | :dir => @north}}

  def move({:ok, %{:dir => @north}} = rover, @right, grid), do: {:ok, %{elem(rover, 1) | :dir => @east}}
  def move({:ok, %{:dir => @east}} = rover, @right, grid), do: {:ok, %{elem(rover, 1) | :dir => @south}}
  def move({:ok, %{:dir => @south}} = rover, @right, grid), do: {:ok, %{elem(rover, 1) | :dir => @west}}
  def move({:ok, %{:dir => @west}} = rover, @right, grid), do: {:ok, %{elem(rover, 1) | :dir => @north}}

  def move({:ok, %{:dir => @north, :y => y}} = rover, @move, [row, col] = grid) when y + 1 <= row, do: {:ok, %{elem(rover, 1) | :y => y + 1}}
  def move({:ok, %{:dir => @west, :x => x}} = rover, @move, [row, col] = grid) when x - 1 >= 0, do: {:ok, %{elem(rover, 1) | :x => x - 1}}
  def move({:ok, %{:dir => @south, :y => y}} = rover, @move, [row, col] = grid) when y - 1 >= 0, do: {:ok, %{elem(rover, 1) | :y => y - 1}}
  def move({:ok, %{:dir => @east, :x => x}} = rover, @move, [row, col] = grid) when x + 1 <= col, do: {:ok, %{elem(rover, 1) | :x => x + 1}}

  def move(_, @move, _), do: {:error, "Rover out of Bound!"}
  def move(rover, _, _), do: {:error, "Invalid Move or Case! Supported moves are 'L', 'R', 'M'."}

  @doc """
    Executes the instructions

    ## Parameters

      - rover data: map type
      - instructions: list type
      - grid: list type

    ## Example

    iex> Rover.execute({:ok, %{:dir => "E", :x => 1, :y => 5}},["L","R"], [6,6])
    {:ok, %{:dir => "E", :x => 1, :y => 5}}
    """
  @spec execute(rover :: tuple(), instructions :: list(), grid :: list()) :: tuple()
  def execute(rover, instructions, _grid) when length(instructions) == 0, do: rover
  def execute(rover, [first | rest] = instructions, grid), do: execute(move(rover, first, grid), rest, grid)
end
