defmodule GridTest do
  use ExUnit.Case
  doctest Grid
  doctest Parser

  describe "Grid" do
    test "should return Invalid if invalid grid values passed" do
      grid = Parser.grid_parser("5")
      assert grid == {:error, "Invalid grid size!"}

      grid = Parser.grid_parser("5 5 5")
      assert grid == {:error, "Invalid grid size!"}
    end

    test "should return tuple with grid array with correct grid initialization" do
      grid = Parser.grid_parser("5 5")
      assert grid == {:ok, [5, 5]}

      grid = Parser.grid_parser("3 2")
      assert grid == {:ok, [3, 2]}
    end
  end
end
