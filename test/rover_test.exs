defmodule RoverTest do
  use ExUnit.Case
  doctest Rover
  doctest Parser

  describe "Rover" do
    test "check for correct rover initialization" do
      grid = Parser.grid_parser("5 5") |> elem(1)
      rover = Parser.rover_parser("1 2 W", grid)
      assert rover == {:ok, %{dir: "W", x: 1, y: 2}}

      rover = Parser.rover_parser("1 0 S", grid)
      assert rover == {:ok, %{dir: "S", x: 1, y: 0}}
    end

    test "check for incorrect rover initialization with wrong arguments" do
      rover = Parser.rover_parser("1 2 W",[])
      assert rover == {:error, "Invalid Rover Coordinates!"}

      rover = Parser.rover_parser("1 2 W",[2,3,5])
      assert rover == {:error, "Invalid Rover Coordinates!"}

      rover = Parser.rover_parser("1 2 W",[5])
      assert rover == {:error, "Invalid Rover Coordinates!"}

      rover = Parser.rover_parser('1 2 W',[])
      assert rover == {:error, "Invalid Rover Coordinates!"}
    end

    test "check for invalid rover direction" do
      grid = Parser.grid_parser("1 2") |> elem(1)
      rover = Parser.rover_parser("0 0 A", grid)
      assert rover == {:error, "Invalid Direction or Case! Supported directions are 'N', 'S', 'E', 'W'."}

      rover = Parser.rover_parser("0 0 n", grid)
      assert rover == {:error, "Invalid Direction or Case! Supported directions are 'N', 'S', 'E', 'W'."}
    end

    test "check for invalid rover coordinates" do
      grid = Parser.grid_parser("2 2") |> elem(1)
      rover = Parser.rover_parser("-1 2 N", grid)
      assert rover == {:error, "Invalid Rover Coordinates!"}

      rover = Parser.rover_parser("3 2 N", grid)
      assert rover == {:error, "Invalid Rover Coordinates!"}
    end

    test "check for rover out of bound" do
      grid = Parser.grid_parser("0 0") |> elem(1)
      rover = Parser.rover_parser("0 0 N", grid)
      ins = Parser.instruction_parser("MM") |> elem(1)
      assert Rover.execute(rover, ins, grid) == {:error, "Rover out of Bound!"}

      grid = Parser.grid_parser("5 8") |> elem(1)
      rover = Parser.rover_parser("6 3 N", grid)
      ins = Parser.instruction_parser("MMMMM") |> elem(1)
      assert Rover.execute(rover, ins, grid) == {:error, "Rover out of Bound!"}
    end

    test "check for zero instructions for rover" do
      grid = Parser.grid_parser("3 3") |> elem(1)
      rover = Parser.rover_parser("1 1 N", grid)
      ins = Parser.instruction_parser("") |> elem(1)
      assert  Rover.execute(rover, ins, grid) == {:ok, %{dir: "N", x: 1, y: 1}}
    end

    test "check for valid rover L movement" do
      grid = Parser.grid_parser("5 5") |> elem(1)
      rover = Parser.rover_parser("1 2 N",grid)
      ins = Parser.instruction_parser("L") |> elem(1)
      assert  Rover.execute(rover, ins, grid) == {:ok, %{dir: "W", x: 1, y: 2}}

      rover = Parser.rover_parser("1 2 W",grid)
      ins = Parser.instruction_parser("L") |> elem(1)
      assert  Rover.execute(rover, ins, grid) == {:ok, %{dir: "S", x: 1, y: 2}}

      rover = Parser.rover_parser("1 2 S",grid)
      ins = Parser.instruction_parser("L") |> elem(1)
      assert  Rover.execute(rover, ins, grid) == {:ok, %{dir: "E", x: 1, y: 2}}

      rover = Parser.rover_parser("1 2 E",grid)
      ins = Parser.instruction_parser("L") |> elem(1)
      assert  Rover.execute(rover, ins, grid) == {:ok, %{dir: "N", x: 1, y: 2}}
    end

    test "check for valid rover R movement" do
      grid = Parser.grid_parser("5 5") |> elem(1)
      rover = Parser.rover_parser("1 2 N",grid)
      ins = Parser.instruction_parser("R") |> elem(1)
      assert  Rover.execute(rover, ins, grid) == {:ok, %{dir: "E", x: 1, y: 2}}

      rover = Parser.rover_parser("1 2 W",grid)
      ins = Parser.instruction_parser("R") |> elem(1)
      assert  Rover.execute(rover, ins, grid) == {:ok, %{dir: "N", x: 1, y: 2}}

      rover = Parser.rover_parser("1 2 S",grid)
      ins = Parser.instruction_parser("R") |> elem(1)
      assert  Rover.execute(rover, ins, grid) == {:ok, %{dir: "W", x: 1, y: 2}}

      rover = Parser.rover_parser("1 2 E",grid)
      ins = Parser.instruction_parser("R") |> elem(1)
      assert  Rover.execute(rover, ins, grid) == {:ok, %{dir: "S", x: 1, y: 2}}
    end

    test "check for valid rover M movement for each direction" do
      grid = Parser.grid_parser("5 5") |> elem(1)
      rover = Parser.rover_parser("1 2 N",grid)
      ins = Parser.instruction_parser("M") |> elem(1)
      assert  Rover.execute(rover, ins, grid) == {:ok, %{dir: "N", x: 1, y: 3}}

      rover = Parser.rover_parser("1 2 S",grid)
      ins = Parser.instruction_parser("M") |> elem(1)
      assert  Rover.execute(rover, ins, grid) == {:ok, %{dir: "S", x: 1, y: 1}}

      rover = Parser.rover_parser("1 2 E",grid)
      ins = Parser.instruction_parser("M") |> elem(1)
      assert  Rover.execute(rover, ins, grid) == {:ok, %{dir: "E", x: 2, y: 2}}

      rover = Parser.rover_parser("1 2 W",grid)
      ins = Parser.instruction_parser("M") |> elem(1)
      assert  Rover.execute(rover, ins, grid) == {:ok, %{dir: "W", x: 0, y: 2}}
    end

    test "check for correct rover movements" do
      grid = Parser.grid_parser("5 5") |> elem(1)
      rover = Parser.rover_parser("1 2 N",grid)
      ins = Parser.instruction_parser("LMLMLMLMM") |> elem(1)
      assert  Rover.execute(rover, ins, grid) == {:ok, %{dir: "N", x: 1, y: 3}}

      ins = Parser.instruction_parser("MMRMMRMRRM") |> elem(1)
      assert  Rover.execute(rover, ins, grid) == {:ok, %{dir: "N", x: 3, y: 4}}

      ins = Parser.instruction_parser("LRLRLRLLMRM") |> elem(1)
      assert  Rover.execute(rover, ins, grid) == {:ok, %{dir: "W", x: 0, y: 1}}
    end

    test "check for incorrect rover movements" do
      grid = Parser.grid_parser("3 3") |> elem(1)
      rover = Parser.rover_parser("0 2 N",grid)
      ins = Parser.instruction_parser("m") |> elem(1)
      assert  Rover.execute(rover, ins, grid) == {:error, "Invalid Move or Case! Supported moves are 'L', 'R', 'M'."}

      ins = Parser.instruction_parser("LRMm") |> elem(1)
      assert  Rover.execute(rover, ins, grid) == {:error, "Invalid Move or Case! Supported moves are 'L', 'R', 'M'."}

      ins = Parser.instruction_parser("lrm") |> elem(1)
      assert  Rover.execute(rover, ins, grid) == {:error, "Invalid Move or Case! Supported moves are 'L', 'R', 'M'."}
    end
  end
end
